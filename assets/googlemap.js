// Initialize and add the map
function initMap() {
  // The location of Uluru
  const uluru = { lat: -37.82668, lng: 145.02047 };

  const mapstyle = [
    {
      featureType: "all",
      elementType: "labels.text.fill",
      stylers: [
        {
          saturation: 36,
        },
        {
          color: "#333333",
        },
        {
          lightness: 40,
        },
      ],
    },
    {
      featureType: "all",
      elementType: "labels.text.stroke",
      stylers: [
        {
          visibility: "on",
        },
        {
          color: "#ffffff",
        },
        {
          lightness: 16,
        },
      ],
    },
    {
      featureType: "all",
      elementType: "labels.icon",
      stylers: [
        {
          visibility: "off",
        },
      ],
    },
    {
      featureType: "administrative",
      elementType: "geometry.fill",
      stylers: [
        {
          color: "#fefefe",
        },
        {
          lightness: 20,
        },
      ],
    },
    {
      featureType: "administrative",
      elementType: "geometry.stroke",
      stylers: [
        {
          color: "#fefefe",
        },
        {
          lightness: 17,
        },
        {
          weight: 1.2,
        },
      ],
    },
    {
      featureType: "landscape",
      elementType: "geometry",
      stylers: [
        {
          color: "#f5f5f5",
        },
        {
          lightness: 20,
        },
      ],
    },
    {
      featureType: "landscape.man_made",
      elementType: "geometry.fill",
      stylers: [
        {
          color: "#c8d8da",
        },
      ],
    },
    {
      featureType: "landscape.natural.landcover",
      elementType: "geometry.fill",
      stylers: [
        {
          color: "#e7f7fc",
        },
      ],
    },
    {
      featureType: "landscape.natural.landcover",
      elementType: "geometry.stroke",
      stylers: [
        {
          visibility: "on",
        },
        {
          color: "#ff0000",
        },
      ],
    },
    {
      featureType: "landscape.natural.landcover",
      elementType: "labels.text.fill",
      stylers: [
        {
          visibility: "off",
        },
      ],
    },
    {
      featureType: "landscape.natural.terrain",
      elementType: "labels.text.fill",
      stylers: [
        {
          visibility: "off",
        },
      ],
    },
    {
      featureType: "poi",
      elementType: "geometry",
      stylers: [
        {
          color: "#f5f5f5",
        },
        {
          lightness: 21,
        },
      ],
    },
    {
      featureType: "poi.park",
      elementType: "geometry",
      stylers: [
        {
          color: "#dedede",
        },
        {
          lightness: 21,
        },
      ],
    },
    {
      featureType: "road.highway",
      elementType: "geometry.fill",
      stylers: [
        {
          color: "#c5d3d6",
        },
        {
          lightness: 17,
        },
      ],
    },
    {
      featureType: "road.highway",
      elementType: "geometry.stroke",
      stylers: [
        {
          color: "#c5d3d6",
        },
        {
          lightness: 29,
        },
        {
          weight: 0.2,
        },
      ],
    },
    {
      featureType: "road.highway.controlled_access",
      elementType: "geometry.fill",
      stylers: [
        {
          color: "#ccdbcd",
        },
      ],
    },
    {
      featureType: "road.arterial",
      elementType: "geometry",
      stylers: [
        {
          color: "#ffffff",
        },
        {
          lightness: 18,
        },
      ],
    },
    {
      featureType: "road.arterial",
      elementType: "geometry.fill",
      stylers: [
        {
          color: "#ccdbcd",
        },
      ],
    },
    {
      featureType: "road.local",
      elementType: "geometry",
      stylers: [
        {
          color: "#ffffff",
        },
        {
          lightness: 16,
        },
      ],
    },
    {
      featureType: "transit",
      elementType: "geometry",
      stylers: [
        {
          color: "#f2f2f2",
        },
        {
          lightness: 19,
        },
      ],
    },
    {
      featureType: "transit.line",
      elementType: "geometry.fill",
      stylers: [
        {
          color: "#8c909c",
        },
      ],
    },
    {
      featureType: "water",
      elementType: "geometry",
      stylers: [
        {
          color: "#e9e9e9",
        },
        {
          lightness: 17,
        },
      ],
    },
  ];

  const iconImg =
    "http://autocarinsurance.dilatedigital.com.au/wp-content/themes/autocardivi/assets/pin.png";

  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 16,
    center: uluru,
    styles: mapstyle,
    zoomControl: true,
    mapTypeControl: false,
    scaleControl: false,
    streetViewControl: false,
    rotateControl: false,
    fullscreenControl: false,
  });
  // The marker, positioned at Uluru
  const marker = new google.maps.Marker({
    position: uluru,
    map: map,
    icon: iconImg,
  });

  const triangleCoords = [
    {
      lat: -31.555362952398767,
      lng: 115.62838074998537,
    },
    {
      lat: -31.551852198765474,
      lng: 115.98955628709474,
    },
    {
      lat: -31.80778953140521,
      lng: 116.00191590623537,
    },
    {
      lat: -31.808956575637943,
      lng: 116.22026917771974,
    },
    {
      lat: -31.808956575637943,
      lng: 116.28069398240724,
    },
    {
      lat: -31.877786078737962,
      lng: 116.27657410936037,
    },
    {
      lat: -31.95239048824726,
      lng: 116.18387696580568,
    },
    {
      lat: -31.948894758336067,
      lng: 116.09461304979006,
    },
    {
      lat: -32.039740483344424,
      lng: 116.09461304979006,
    },
    {
      lat: -32.071166105415536,
      lng: 116.06714722947756,
    },
    {
      lat: -32.068838651739775,
      lng: 116.02457520799318,
    },
    {
      lat: -32.12235509435985,
      lng: 116.02457520799318,
    },
    {
      lat: -32.127007216185184,
      lng: 116.1248254521338,
    },
    {
      lat: -32.19443631965267,
      lng: 116.12619874314943,
    },
    {
      lat: -32.242071604259806,
      lng: 116.08774659471193,
    },
    {
      lat: -32.302451170975615,
      lng: 115.92569825486818,
    },
    {
      lat: -32.334946596282634,
      lng: 115.86252686814943,
    },
    {
      lat: -32.47176339158238,
      lng: 115.86252686814943,
    },
    {
      lat: -32.532505435147826,
      lng: 115.81359607999404,
    },
    {
      lat: -32.5348210114921,
      lng: 115.71746570890029,
    },
    {
      lat: -32.513978675563486,
      lng: 115.72295887296279,
    },
    {
      lat: -32.48618137530695,
      lng: 115.73669178311904,
    },
    {
      lat: -32.4328792106848,
      lng: 115.74905140225967,
    },
    {
      lat: -32.3853441854802,
      lng: 115.73531849210342,
    },
    {
      lat: -32.36678714178099,
      lng: 115.71609241788467,
    },
    {
      lat: -32.3435854792862,
      lng: 115.74767811124404,
    },
    {
      lat: -32.31341442154378,
      lng: 115.73531849210342,
    },
    {
      lat: -32.30296825154593,
      lng: 115.69274647061904,
    },
    {
      lat: -32.27626701296305,
      lng: 115.69274647061904,
    },
    {
      lat: -32.265816562024675,
      lng: 115.67764026944717,
    },
    {
      lat: -32.27278366305636,
      lng: 115.73531849210342,
    },
    {
      lat: -32.23561961054613,
      lng: 115.75591785733779,
    },
    {
      lat: -32.19030536122427,
      lng: 115.77377064054092,
    },
    {
      lat: -32.16124589259145,
      lng: 115.76141102140029,
    },
    {
      lat: -32.13566589160611,
      lng: 115.76141102140029,
    },
    {
      lat: -32.136828774622046,
      lng: 115.72982532804092,
    },
    {
      lat: -32.122873199965646,
      lng: 115.76003773038467,
    },
    {
      lat: -32.07750284345407,
      lng: 115.75317127530654,
    },
    {
      lat: -32.05539117414071,
      lng: 115.72982532804092,
    },
    {
      lat: -32.03909494484698,
      lng: 115.72570545499404,
    },
    {
      lat: -32.03210995840968,
      lng: 115.74767811124404,
    },
    {
      lat: -31.96828070219839,
      lng: 115.76082953956556,
    },
    {
      lat: -31.926330421227718,
      lng: 115.75258979347181,
    },
    {
      lat: -31.848205321452763,
      lng: 115.75533637550306,
    },
    {
      lat: -31.81553639839572,
      lng: 115.73061713722181,
    },
    {
      lat: -31.80503321769841,
      lng: 115.71276435401869,
    },
    {
      lat: -31.77818633109434,
      lng: 115.73611030128431,
    },
    {
      lat: -31.71395565278444,
      lng: 115.70727118995619,
    },
    {
      lat: -31.657863399942833,
      lng: 115.67568549659681,
    },
    {
      lat: -31.62044974028456,
      lng: 115.66195258644056,
    },
    {
      lat: -31.587700443679946,
      lng: 115.64272651222181,
    },
    {
      lat: -31.555362952398767,
      lng: 115.62838074998537,
    },
  ];

  const flightPath = new google.maps.Polyline({
    path: triangleCoords,
    geodesic: true,
    strokeColor: "#FFFFFF",
    strokeOpacity: 1.0,
    strokeWeight: 1,
  });
  flightPath.setMap(map);
}
