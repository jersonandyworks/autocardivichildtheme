<?php
function my_theme_enqueue_styles()
{
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style('bigpicture-main-style', get_stylesheet_directory_uri() . '/bigpicture/main.css');
}
add_action('wp_enqueue_scripts', 'my_theme_enqueue_styles');

function slick_slider_custom_plugin()
{
    wp_enqueue_style('slick-theme', get_stylesheet_directory_uri() . '/slickjs/slick.css');
    wp_register_script('slick-js', 'http://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js');
    wp_register_script('slick-function-js', get_stylesheet_directory_uri() . '/slickjs/slick.js');

    wp_register_script('bigpicture-js', get_stylesheet_directory_uri() . '/bigpicture/BigPicture.js');
    wp_register_script('bigpicture-main-js', get_stylesheet_directory_uri() . '/bigpicture/main.js');

    // wp_enqueue_script('jquery30');
    wp_enqueue_script('bigpicture-js');
    wp_enqueue_script('bigpicture-main-js');
    wp_enqueue_script('slick-js');
    wp_enqueue_script('slick-function-js');
    wp_enqueue_script('googlemaps');
    wp_enqueue_script('custommapscript');

}

function custom_map()
{
    wp_register_script('custommapscript', get_stylesheet_directory_uri() . '/assets/googlemap.js');
    wp_register_script('googlemaps', 'https://maps.googleapis.com/maps/api/js?&key=AIzaSyClDGFnyszA_dpXvvYW63HqTSOvz04JJps&callback=initMap', array(), '', true);

    wp_enqueue_script('googlemaps');
    wp_enqueue_script('custommapscript');
}

add_action('init', 'custom_map', 5);
add_action('wp_footer', 'slick_slider_custom_plugin', 10);

function slider_custom_post_type($atts)
{

//    $a = shortcode_atts( ['mobile' => true], $atts);
    //    $today = date('Ymd');

    $args = array(
        'post_type' => 'autocar_slider',
        'orderby' => 'date',
        'order' => 'ASC',
    );

    $the_query = new WP_Query($args);
    $result = "<div class='slide-slick'>";
    if ($the_query->have_posts()) {
        while ($the_query->have_posts()) {
            $the_query->the_post();
            $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full');
            $result .= '<div class="auto_car_slider_bg" style=background-image:url("' . $featured_img_url . '")>';
            $result .= '<div class="auto_car_slider_title">' . get_field('title') . '</div>';
            $result .= '<div class="auto_car_slider_description">' . get_field('slider_description') . '</div>';
            $result .= '<div class="auto_car_slider_button"><a class="auto_car_slider_link" href="' . get_field('button_link') . '">' . get_field('slider_button_text') . '</a></div>';
            $result .= '</div>';
        }
    }
    $result .= "</div>";

    return $result;

}
add_shortcode('autocar-slider', 'slider_custom_post_type');
// test

function show_category_name()
{
    global $post;
    $category_name = wp_get_post_terms(get_the_ID(), 'category')[0]->name;
    return $category_name;
}

add_shortcode('auto_post_category', 'show_category_name');

function client_comment_slider()
{
    $args = array(
        'post_type' => 'clients_comments',
        'orderby' => 'date',
        'order' => 'ASC',
    );
    $the_query = new WP_Query($args);
    $result = '<div class="comment-slider">';
    if ($the_query->have_posts()) {

        while ($the_query->have_posts()) {
            $the_query->the_post();

            $result .= '<div class="comment-box">';
            // $result .= "<h3>HELLO</h3>";
            $result .= '<div class="testi-box-1">';
            $result .= '<p class="comment-text">' . get_field('clients_comment') . '</p>';
            $result .= '<img src="http://autocarinsurance.dilatedigital.com.au/wp-content/uploads/2022/03/staaaaaaaaaar.png">';
            $result .= '<h3 class="author">' . get_field('fullname') . '</h3>';
            $result .= '<p>' . get_field('description') . '</p>';
            $result .= '</div>';
            $result .= '</div>';

        }

    }
    $result .= '</div>';
    return $result;
}

add_shortcode('autocar-comment-slider', 'client_comment_slider');

function breadcrumbs_searchbar($atts)
{
    global $post;
    $a = shortcode_atts(['page' => false, 'searchbar' => 1], $atts);
    $result = "<div class='breadcrumbs_searchbar'>";
    if ($a['page']) {
        $result .= "<div id='breadcrumbs'>";
        $result .= "<a href='" . get_permalink($post->post_parent) . "'>" . get_the_title($post->post_parent) . " <i class='icn-caret-forward'></i></a>";
        $result .= "<a>" . $post->post_title . "</a>";
        $result .= "</div>";
    } else {
        $result .= "<div id='breadcrumbs'>";
        $result .= "<a href='http://autocarinsurance.dilatedigital.com.au/index.php/get-to-know-us/'>About <i class='icn-caret-forward'></i></a>";
        $result .= "<a href='http://autocarinsurance.dilatedigital.com.au/index.php/auto-car-finance-blog/'>Auto Car Finance Blog <i class='icn-caret-forward'></i></a>";
        $result .= "<a href='http://autocarinsurance.dilatedigital.com.au/index.php/auto-car-finance-blog/'>" . show_category_name() . "</a>";
        $result .= "</div>";
    }

    if ($a['searchbar'] == 1) {
        $result .= "<div class='searchbar'>";
        $result .= "<form id='searchform' method='get' action='" . esc_url(home_url('/')) . "'>";
        $result .= "  <input type='text' class='search-field' name='s' placeholder='Search Topic' value='" . get_search_query() . "'>";
        $result .= "</form>";
        $result .= "</div>";
    }

    $result .= "</div>";

    return $result;
}

add_shortcode('breadcrumbsearchbar', 'breadcrumbs_searchbar');

function getRelatedPosts($atts)
{
    $a = shortcode_atts(['category' => 'uncategorized', 'limit' => 2, 'single' => false, 'continue' => false, 'isarchive' => false, 'ishome' => false], $atts);

    $current_post_Id = get_the_ID();
    $html = '<div class="related-posts-container">';
    $exculdedPostIds = [$current_post_Id];
    $queryArgs = array(
        'post_type' => 'post',
        'category_name' => $a['category'],
        'orderby' => 'rand',
        'post__not_in' => [$current_post_Id],
        'posts_per_page ' => $a['limit'],
        '_randomize_posts_count' => $a['limit'],
    );

    if ($a['category'] === "all") {

        $queryArgs = array(
            'post_type' => 'post',
            'posts_per_page ' => $a['limit'],
            'posts_per_archive_page' => $a['limit'],
            'nopaging' => false,
            'order' => 'DESC',
            'offset' => $a['continue'] ? 1 : 0,
        );

        if ($a['isarchive']) {
            $category = get_queried_object();
            $queryArgs = array(
                'post_type' => 'post',
                'category__in' => $category->term_id,
                'nopaging' => false,
                'order' => 'DESC',
            );

        }
    }

    $the_query = new WP_Query($queryArgs);
    if ($a['continue']) {
        $i = 1;
    }
    if ($the_query->have_posts()) {
        while ($the_query->have_posts() && $i < $a['limit']) {
            $the_query->the_post();
            $postID = get_the_ID();
            $categories = get_the_category($postID);
            $post_url = get_permalink($postID);
            $cats = array();
            foreach ($categories as $category) {
                array_push($cats, ucwords($category->name));
            }
            $className = "img-featured";
            $image = "";
            $featured_img_url = get_the_post_thumbnail_url($postID, 'full');
            if (!$featured_img_url) {
                $image = get_stylesheet_directory_uri() . '/assets/image-placeholder.jpg';
                $className = "img-placeholder";
            } else {
                $image = $featured_img_url;
            }
            $postCounter = $i + 1;
            $singlePostClass = "";
            if ($a['single']) {
                $singlePostClass = "single-post";
            }

            if ($a['ishome']) {
                $singlePostClass = "home-post";
            }

            $html .= '<div class="related-post-item ' . $singlePostClass . ' ">';
            $html .= '<a href="' . $post_url . '">';
            $html .= '<div class="related-post-item-count">' . str_pad($postCounter, 2, '0', STR_PAD_LEFT) . '</div>';
            $html .= '<div class="related-post-item-category">';
            foreach ($cats as $cat) {
                $categoryColor = "#2e9fd7";
                switch ($cat) {
                    case 'Debtor finance':
                        $categoryColor = "#00294f";
                        break;
                    case 'Buying and Selling':
                        $categoryColor = "#ffae00";
                        break;
                    default:
                        $categoryColor = "#2e9fd7";
                        break;
                }
                $html .= '<div class="cat-items" style="background-color:' . $categoryColor . '">' . $cat . '</div>';
                $html .= '</div>';
            }
            $html .= '<img src="' . $image . '" class="' . $className . '">';
            //. ' | by ' . get_the_author()
            // $html .= '<div class="related-post-tag"><img src="http://leeming.dilatedigital.com.au/wp-content/uploads/2021/11/tag.png"/> ' . implode(",", $cats) . '</div>';
            $html .= '<div class="related-post-title">' . get_the_title() . '</div>';
            $html .= '<div class="related-post-date">' . get_the_date('m/d/Y') . '</div>';
            $html .= '<div class="related-post-excerpt">' . substr(get_the_excerpt($postID), 0, 150) . '... </div>';
            $html .= '<button>Read More <i class="icn-caret-long-forward"></i> </button>';
            $html .= '</a>';
            $html .= '</div>';
            $i++;
        }

    }
    $html .= "</div>";
    return $html;
}
add_shortcode("related_posts", 'getRelatedPosts');

function category_name_shortcode()
{
    $current_category = get_queried_object();
    $html = "";
    $html .= "<ul class='category-links'>";
    foreach (get_categories() as $category) {
        if ($current_category->term_id !== $category->term_id) {
            $html .= '<li><a href="' . esc_url(get_category_link($category->term_id)) . '">' . $category->name . '</a></li>';
        }
    }
    $html .= "</ul>";
    return $html;
}
add_shortcode('post_category', 'category_name_shortcode');
