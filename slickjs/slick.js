(function ($) {
  if ($(".related-posts-container").children().length >= 2) {
    $("#related-articles-row").show();
  } else {
    $("#related-articles-row").hide();
  }

  $("#slider-video-section").append("<div id='slider-video-slides'></div>");
  $("#slider-video-slides")
    .addClass("slide-video-slick")
    .append($("#slider-video-section").children());
  $("#referral-form-section").hide();
  $slider = $(".slide-slick");
  $slider.on("init", function (event, slick) {
    $("#sliderCount").text("1 / " + slick.slideCount);
  });
  $slider.on("afterChange", function (event, slick) {
    $("#sliderCount").text(
      slick.slickCurrentSlide() + 1 + " / " + slick.slideCount
    );
  });

  $slider.slick({
    dots: false,
    infinite: true,
    arrows: true,
    appendArrows: $(".slide-slick .slider-navs"),
    prevArrow: $(".slick-prev"),
    nextArrow: $(".slick-next"),
    speed: 300,
    slidesToShow: 1,
  });

  //   TESTIMONIAL SLIDER
  $testimonialSlider = $(".comment-slider");
  $testimonialSlider.on("init", function (event, slick) {
    var currentSlide = slick.slickCurrentSlide() + 1;
    var slideCount = slick.slideCount;
    currentSlide = currentSlide.toString().padStart(2, "0");
    slideCount = slideCount.toString().padStart(2, "0");
    $(".prev-num").text(currentSlide);
    $(".next-num").text(slideCount);
  });
  $testimonialSlider.on("afterChange", function (event, slick) {
    var currentSlide = slick.slickCurrentSlide() + 1;
    var slideCount = slick.slideCount;
    currentSlide = currentSlide.toString().padStart(2, "0");
    slideCount = slideCount.toString().padStart(2, "0");
    $(".prev-num").text(currentSlide);
    $(".next-num").text(slideCount);
  });

  $testimonialSlider.slick({
    dots: true,
    draggable: true,
    arrows: true,
    appendArrows: $(".comment-navs"),
    prevArrow: $(".comment-prev"),
    nextArrow: $(".comment-next"),
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1,
  });

  // $("#vidBox").prependTo("#page-container");

  $(".related-post-item a").mouseover(function (event) {
    $(this).removeClass("AnimationOut");
    $(this).addClass("AnimationIn");
  });

  $(".related-post-item a").mouseout(function (event) {
    $(this).removeClass("AnimationIn");
    $(this).addClass("AnimationOut");
  });

  // VIDEO SLIDER
  $videoSlider = $(".slide-video-slick");
  $videoSlider.on("init", function (event, slick) {
    $("#sliderCount").text("1 / " + slick.slideCount);
    var currentSlide = slick.slickCurrentSlide() + 1;
    var slideCount = slick.slideCount;
    currentSlide = currentSlide.toString().padStart(2, "0");
    slideCount = slideCount.toString().padStart(2, "0");
    $(".prev-num").text(currentSlide);
    $(".next-num").text(slideCount);
  });
  $videoSlider.on("afterChange", function (event, slick) {
    $("#sliderCount").text(
      slick.slickCurrentSlide() + 1 + " / " + slick.slideCount
    );
    var currentSlide = slick.slickCurrentSlide() + 1;
    var slideCount = slick.slideCount;
    currentSlide = currentSlide.toString().padStart(2, "0");
    slideCount = slideCount.toString().padStart(2, "0");
    $(".prev-num").text(currentSlide);
    $(".next-num").text(slideCount);
  });

  $videoSlider.slick({
    dots: false,
    infinite: true,
    arrows: true,
    appendArrows: $(".comment-navs"),
    prevArrow: $(".comment-prev"),
    nextArrow: $(".comment-next"),
    speed: 300,
    slidesToShow: 1,
  });

  // 3 CAROUSEL SLIDER
  $videoSliderCarousel = $(".slide-video-carousel-slick");
  $videoSliderCarousel.on("init", function (event, slick) {
    $("#sliderCount").text("1 / " + slick.slideCount);
    var currentSlide = slick.slickCurrentSlide() + 1;
    var slideCount = slick.slideCount;
    currentSlide = currentSlide.toString().padStart(2, "0");
    slideCount = slideCount.toString().padStart(2, "0");
    $(".prev-num").text(currentSlide);
    $(".next-num").text(slideCount);
  });
  $videoSliderCarousel.on("afterChange", function (event, slick) {
    $("#sliderCount").text(
      slick.slickCurrentSlide() + 1 + " / " + slick.slideCount
    );
    var currentSlide = slick.slickCurrentSlide() + 1;
    var slideCount = slick.slideCount;
    currentSlide = currentSlide.toString().padStart(2, "0");
    slideCount = slideCount.toString().padStart(2, "0");
    $(".prev-num").text(currentSlide);
    $(".next-num").text(slideCount);
  });

  $videoSliderCarousel.slick({
    dots: false,
    infinite: false,
    arrows: true,
    appendArrows: $(".comment-navs"),
    prevArrow: $(".comment-prev"),
    nextArrow: $(".comment-next"),
    speed: 300,
    slidesToShow: 3,
  });

  $(".et_pb_title_meta_container").text(
    $(".et_pb_title_meta_container").text().replace("by", "Author:")
  );

  $("#refer-btn").click(function (e) {
    e.preventDefault();
    $("#referral-form-section").show();
    console.log("refer");
    $([document.documentElement, document.body]).animate(
      {
        scrollTop: $("#referral-form-section").offset().top,
      },
      2000
    );
  });

  // JS HOVER IMAGE
  $(".txt-hover-img").mouseover(function () {
    $(this).removeClass("fadeOut");
    $(this).addClass("fadeIn");
  });
  $(".txt-hover-img").mouseout(function () {
    $(this).removeClass("fadeIn");
    $(this).addClass("fadeOut");
  });

  $("#show-current-novated-lease-more-years").hide();
  $("#select-current-novated").on("change", function () {
    console.log(this.value);
    if (this.value === "Yes") {
      $("#show-current-novated-lease-more-years").show();
    } else {
      $("#show-current-novated-lease-more-years").hide();
    }
  });

  $("#blur-submit-btn button").on("click", function () {
    var url =
      "http://autocarinsurance.dilatedigital.com.au/wp-content/uploads/2022/06/Auto-Car-Finance-_-Bible-Handbook.pdf";
    // window.open(url, "_blank");
  });

  var checkBox = $("#personal-check-same input");
  var personalAddress = $("#personal-address");
  var personalSuburb = $("#personal-suburb");
  var personalState = $("#personal-state");
  var personalPostCode = $("#personal-postcode");
  var mailingAddress = $("#mailing-address");
  var mailingSubUrb = $("#mailing-suburb");
  var mailingState = $("#mailing-state");
  var mailingPostCode = $("#mailing-postcode");

  checkBox.on("change", function () {
    if ($(this).is(":checked")) {
      mailingAddress.val(personalAddress.val());
      mailingSubUrb.val(personalSuburb.val());
      mailingState.val(personalState.val());
      mailingPostCode.val(personalPostCode.val());
    } else {
      mailingAddress.val("");
      mailingSubUrb.val("");
      mailingState.val("");
      mailingPostCode.val("");
    }
  });

  $("#auto-car-field-button").on("click", function () {
    console.log("submitting package setup!");
    var companyField = $("#companyname").val();
    console.log("company field: " + companyField);
    $("#companyname").val(companyField);
    setTimeout(() => {
      window.scrollTo({ top: 0, behavior: "smooth" });
    }, 3000);
  });
})(jQuery);

// (document).ready(function () {
// });
