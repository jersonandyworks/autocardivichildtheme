(function () {
  function setClickHandler(id, fn) {
    document.getElementById(id).onclick = fn;
  }

  setClickHandler("video_container", function (e) {
    var className = e.target.className;
    if (~className.indexOf("htmlvid")) {
      BigPicture({
        el: e.target,
        vidSrc: e.target.getAttribute("vidSrc"),
      });
    }
  });

  setClickHandler("video_container2", function (e) {
    var className = e.target.className;
    if (~className.indexOf("htmlvid")) {
      BigPicture({
        el: e.target,
        vidSrc: e.target.getAttribute("vidSrc"),
      });
    }
  });

  setClickHandler("video_container3", function (e) {
    var className = e.target.className;
    if (~className.indexOf("htmlvid")) {
      BigPicture({
        el: e.target,
        vidSrc: e.target.getAttribute("vidSrc"),
      });
    }
  });

  setClickHandler("video_container4", function (e) {
    var className = e.target.className;
    if (~className.indexOf("htmlvid")) {
      BigPicture({
        el: e.target,
        vidSrc: e.target.getAttribute("vidSrc"),
      });
    }
  });
})();
